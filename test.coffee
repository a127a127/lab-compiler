$(->
	# Init
	## Define teacup helper function
	teacup.tc = (template)->
		r = -> template.apply(teacup, arguments)
		r.render = teacup.renderable(r)
		return r
	## Define Textarea
	Textarea = teacup.tc((text, id, rows, cols, opt)->
		@div "#{text}: "
		@textarea $.extend({id, rows, cols}, opt)
	)
	Button = teacup.tc((text)->
		@button {id: text}, text
	)
	## Init UI
	$('#content').html(
		teacup.tc(->
			@div ->
				@div {style: 'display: inline-block'}, ->
					Textarea('Source', 'src', 20, 80)
				@div {style: 'display: inline-block'}, ->
					Textarea('Output', 'out', 20, 80, {readonly:'readonly'})
			@div ->
				Button('compile')
				Button('run')
			@div ->
				Textarea('Message', 'msg', 10, 163, {readonly:'readonly'})
		).render()
	)
	## Init helper functions
	log = do->
		msg_block = $("#msg")
		r = (msg)-> r.inline(msg); r.inline('\n')
		r.inline = (msg)-> msg_block.append("#{msg?.toString?()}")
		return r

	# Main
	$('#run').click(->
		log 'XD'
	)
)
